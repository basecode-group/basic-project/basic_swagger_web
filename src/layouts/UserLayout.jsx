import { DefaultFooter, getMenuData, getPageTitle } from '@ant-design/pro-layout';
import { Helmet } from 'react-helmet';
import { Link } from 'umi';
import React from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import {Icon} from 'antd'
import SelectLang from '@/components/SelectLang';
// import logo from '../assets/logo.svg';
import logo from '../assets/swagger_logo (1).png';
// import logo from '../assets/swagger-ui-logo.svg';
import styles from './UserLayout.less';

const UserLayout = props => {
  const {
    route = {
      routes: [],
    },
  } = props;
  const { routes = [] } = route;
  const {
    children,
    location = {
      pathname: '',
    },
  } = props;
  const { breadcrumb } = getMenuData(routes);
  const title = getPageTitle({
    pathname: location.pathname,
    breadcrumb,
    formatMessage,
    ...props,
  });
  return (
    <>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={title} />
      </Helmet>

      <div className={styles.container}>
        <div className={styles.lang}>
          <SelectLang />
        </div>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
              <Link to="/">
                <img alt="logo" className={styles.logo} src={logo} />
                <span className={styles.title} style={{ fontWeight: 400 }}>LOGIN</span>
              </Link>
            </div>
            <div className={styles.desc}>Swagger-UI 登录管理系统</div>
          </div>
          {children}
        </div>
        <DefaultFooter
          copyright="2020 Bacisic Swagger API文档"
          links={[
            {
              key: 'gitlab',
              title: <Icon type="gitlab" />,
              href: 'https://gitlab.com/basecode-group/basic-project/basic_common',
              blankTarget: true,
            },
          ]}
        />
      </div>
    </>
  );
};

export default connect(({ settings }) => ({ ...settings }))(UserLayout);
