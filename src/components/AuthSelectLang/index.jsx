import React, { Component } from 'react';
import HeaderDropdown from '../HeaderDropdown';
import { Form, Icon, Menu, Modal, Row, Col, Input, Button, message } from 'antd';
import styles from './index.less';
import { connect } from 'dva';
import classNames from 'classnames';


/**
 * 授权
 *
 * @author zhangby
 * @date 26/2/20 9:40 am
 */
class index extends Component {
  state={
    visible: false,
    authorization: localStorage.getItem('token'),
    swaggerAuthUrl: localStorage.getItem('swaggerAuthUrl'),
    swaggerAuthUsername: localStorage.getItem('swaggerAuthUsername'),
    swaggerAuthPassword: localStorage.getItem('swaggerAuthPassword')
  }

  componentWillMount() {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch({
        type: 'swagger/updateAuth',
        authorization: localStorage.getItem('token')
      });
    }
  }

  // 授权
  authSwagger(){
    if (this.state.authorization !== undefined) {
      this.clearAuth();
    } else {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'swagger/updateAuth',
          authorization: this.state.authorization
        });
      }
      message.success("授权成功")
    }
  }

  quickAuth(){
    const { swaggerAuthUrl, swaggerAuthUsername, swaggerAuthPassword } = this.state;
    if (swaggerAuthUrl === undefined || swaggerAuthUrl === null ||
      (swaggerAuthUrl !== undefined && swaggerAuthUrl !== null && swaggerAuthUrl.trim() === "")) {
      message.error("授权地址不能为空");
      return;
    }
    if (swaggerAuthUsername === undefined || swaggerAuthUsername === null ||
      (swaggerAuthUsername !== undefined && swaggerAuthUsername !== null && swaggerAuthUsername.trim() === "")) {
      message.error("登录名不能为空");
      return;
    }
    if (swaggerAuthPassword === undefined || swaggerAuthPassword === null ||
      (swaggerAuthPassword !== undefined && swaggerAuthPassword !== null && swaggerAuthPassword.trim() === "")) {
      message.error("密码不能为空");
      return;
    }
    localStorage.setItem("swaggerAuthUrl", swaggerAuthUrl);
    localStorage.setItem("swaggerAuthUsername", swaggerAuthUsername);
    localStorage.setItem("swaggerAuthPassword", swaggerAuthPassword);
    const { dispatch } = this.props;

    if (dispatch) {
      dispatch({
        type: 'swagger/swaggerAuth',
      });
    }
    message.success("授权成功")
  }

  clearAuth(){
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch({
        type: 'swagger/clearAuth',
      });
    }
    message.success("清除成功")
  }

  updateAuth(val){
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch({
        type: 'swagger/updateAuth',
        authorization: val
      });
    }
  }

  render() {
    const langMenu = (
      <Menu className={styles.menu} >
        <Menu.Item onClick={this.quickAuth.bind(this)}>
          <Icon type="safety"/> 快速授权
        </Menu.Item>
        <Menu.Item onClick={this.clearAuth.bind(this)}>
          <Icon type="logout" /> 清除授权
        </Menu.Item>
        <Menu.Item onClick={this.showModal}>
          <Icon type="form"/> 手动授权
        </Menu.Item>
      </Menu>
    );

    // console.log("state: "+ this.state.authorization)
    // console.log("props: "+ this.props.authorization)
    // console.log("内存："+localStorage.getItem("token"))

    let { authorization } = this.state;
    const { className } = this.props;
    if (authorization !== this.props.authorization) {
      this.setState({ authorization: this.props.authorization })
    }

    return (
      <span>
        <HeaderDropdown overlay={langMenu} placement="bottomRight">
          <span className={classNames(styles.dropDown, className)}
                style={{ color: '#fafafa', marginTop: 2, float: 'right', marginLeft: 10 }}
          >
            <Icon type="setting" title="授权"/>
          </span>
        </HeaderDropdown>

        {/* 弹出窗口 */}
        <Modal
          title="Swagger 授权"
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <Row>
            <Col span={24} style={{ marginBottom: 10 }}>
              <span>授权地址</span>
              <span><Input value={this.state.swaggerAuthUrl}
                           onChange={val => this.setState({ swaggerAuthUrl: val.target.value })}/></span>
            </Col>
            <Col span={11}>
              <span>登录名</span>
              <span><Input value={this.state.swaggerAuthUsername}
                           onChange={val => this.setState({ swaggerAuthUsername: val.target.value })}/></span>
            </Col>
            <Col span={11} offset={2}>
              <span>密码</span>
              <span><Input value={this.state.swaggerAuthPassword}
                           onChange={val => this.setState({ swaggerAuthPassword: val.target.value })}/></span>
            </Col>
            <Col span={24} style={{ marginTop: 20, textAlign: 'center' }}>
              <Button type="primary"
                      icon="vertical-align-bottom"
                      style={{ marginRight: 10 }}
                      onClick={this.quickAuth.bind(this)}
              >
                快速授权
              </Button>
            </Col>
            <Col span={24} style={{ marginTop: 10 }}>
              <span>Authorization</span>
              <span><Input value={authorization}
                           onChange={val => this.updateAuth(val.target.value )}
              /></span>
            </Col>

            <Col span={24} style={{ marginTop: 20, textAlign: 'center' }}>
              <Button type="primary" onClick={this.authSwagger.bind(this)} key={this.state.authorization}>
                {this.state.authorization !== undefined ? "清除" : "授权"}
              </Button>
              <Button style={{ marginLeft: 10 }} onClick={this.handleCancel}>关闭</Button>
            </Col>
          </Row>
        </Modal>
      </span>
    );
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };
}

export default connect(({swagger }) => ({
  authorization: swagger.authorization
}))(index);
// export default Form.create({ name: 'index', })(index);
