import { Icon, Tooltip, Tag, Select } from 'antd';
import React from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import Avatar from './AvatarDropdown';
import HeaderSearch from '../HeaderSearch';
import SelectLang from '../SelectLang';
import AuthSelectLang from '../AuthSelectLang'
import styles from './index.less';
const ENVTagColor = {
  dev: 'orange',
  test: 'green',
  pre: '#87d068',
};

const { Option } = Select;

import { Form } from 'antd';

class GlobalHeaderRight extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;

    if (dispatch) {
      dispatch({
        type: 'swagger/resources',
      });
    }
    this.setState({dispatch})
  }

  handleChange(value){
    const { dispatch } = this.props;

    if (dispatch) {
      dispatch({
        type: 'swagger/setSelected',
        swaggerUrl: value,
      });
    }
  }

  render() {
    const { theme, layout, swaggerData } = this.props;
    let className = styles.right;

    if (theme === 'dark' && layout === 'topmenu') {
      className = `${styles.right}  ${styles.dark}`;
    }

    return (
      <div>
        <div className={className}>
        <span style={{ marginRight: 10 }}>
          <span style={{ color:'#f5f6f7', marginRight: 15 }}>Select a spec</span>
          <Select key={swaggerData.swaggerData}
                  style={{ minWidth: 220 }}
                  defaultValue={swaggerData.selectedSwagger}
                  onChange={this.handleChange.bind(this)}
          >
            {
              swaggerData.swaggerData.map((item,key) => (
                <Option value={item.url} key={key}>{item.name}</Option>
              ))
            }
          </Select>
        </span>
          <Avatar />
          {/*<SelectLang className={styles.action} />*/}
          <AuthSelectLang />
        </div>
      </div>
    )
  }
}

export default connect(({ settings, swagger }) => ({
  theme: settings.navTheme,
  layout: settings.layout,
  swaggerData: swagger,
}))(GlobalHeaderRight);
