import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// const http = "http://47.56.240.158:8020";

export async function resources() {
  // return request('http://47.56.240.158:8020/swagger-resources');
  return request(http+'/swagger/resources/get');
}

export async function swaggerAuth(params) {
  return request(`${http}${params.swaggerAuthUrl}?username=${params.swaggerAuthUsername}&password=${params.swaggerAuthPassword
  }`);
}
