import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function query() {
  return request('/api/users');
}
export async function queryCurrent() {
  // return request('/api/currentUser');
  return request(http+'/swagger/login/verify');
}
export async function queryNotices() {
  return request('/api/notices');
}
